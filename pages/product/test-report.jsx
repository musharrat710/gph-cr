import React, {useEffect} from 'react';
import styled from "styled-components";
import {Container, Row, Col} from "react-bootstrap";
import Link from 'next/link'
import {NextSeo} from "next-seo";
import InnerBanner from "../../components/InnerBanner";
import {useDispatch, useSelector} from "react-redux";
import {useRouter} from "next/router";
import {ApiParamKey} from "../api/network/ApiParamKey";
import {ApiServices} from "../api/network/ApiServices";
import {fetchBannerData, fetchData, fetchPageData} from "../api/redux/product/test-report";
import {wrapper} from "../api/store";
import BrochureList from "../../components/product/BrochureList";

const MyComponent = (props) => {
    const getData = useSelector(state => state.testReducer)
    const dispatch = useDispatch()
    const router = useRouter();


    // api call
    useEffect(() => {
        if (!props.isServer) {
            let api_services_banner = ApiServices.SECTIONS;
            let param_banner = {
                [ApiParamKey.parent_id]: 298,
                [ApiParamKey.get_section]: true,
            }
            let api_services = ApiServices.CHILD_PAGE_BY_ID_WITH_PAGINATION;
            let param = {
                [ApiParamKey.parent_id]: 298,
                [ApiParamKey.per_page]: 6,
            }
            dispatch(fetchData([api_services, param]))
            dispatch(fetchBannerData([api_services_banner, param_banner]))

        }
    }, [props.isServer, router])

    // data refactor


    //banner section
    const bannerData = getData?.dataBanner?.sections?.find(f => f?.page_data?.slug === "test-report-banner");
    const reportData = getData?.data?.list;


    return (
        <StyledComponent>
            <NextSeo
                title={'Test report | GPH Ispat'}
                description={'GPH Ispat Ltd. One of the leaders of Bangladesh in manufacturing steel promises a super strong future and economy with its world –class products. Not only structural bar, but GPH Ispat Ltd. is also one of the producers of low &amp; medium carbon and low alloy steel billets in Bangladesh, the main ingredient of manufacturing graded steel bar. As GPH is ensuring the highest quality products in Bangladesh as per various international and national standards, GPH steel billets and Bars is getting exported to other countries after nourishing national demand. The introduction of GPH Ispat Ltd. has all the potentials to take Bangladesh quite a few steps forward to a stronger, brighter tomorrow'}

            />
            <InnerBanner subtitle={bannerData?.page_data?.subtitle} img={bannerData?.images?.list?.[0]?.full_path}/>
            <BrochureList data={reportData}/>
        </StyledComponent>
    );
};

const StyledComponent = styled.section`

`;
export const getServerSideProps = wrapper.getServerSideProps(
    (store) =>
        async ({req}) => {
            const isServer = !req.url.startsWith("/_next");

            if (isServer) {
                let api_services_banner = ApiServices.SECTIONS;
                let param_banner = {
                    [ApiParamKey.page_id]: 298,
                    [ApiParamKey.get_section]: true,
                }
                let api_services = ApiServices.CHILD_PAGE_BY_ID_WITH_PAGINATION;
                let param = {
                    [ApiParamKey.parent_id]: 298,
                    [ApiParamKey.per_page]: 6,
                    [ApiParamKey.get_section]: true,

                }
                await store.dispatch(fetchData([api_services, param]))
                await store.dispatch(fetchBannerData([api_services_banner, param_banner]))
            }
            return {
                props: {
                    isServer,
                    title: "product-services",
                },
            };
        }
);


export default MyComponent;
