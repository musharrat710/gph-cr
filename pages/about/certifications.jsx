import React, {useEffect} from 'react';
import styled from "styled-components";
import {Container, Row, Col} from "react-bootstrap";
import Link from 'next/link'
import {NextSeo} from "next-seo";
import InnerBanner from "../../components/InnerBanner";
import Certifications from "../../components/about/Certifications";
import {useDispatch, useSelector} from "react-redux";
import {useRouter} from "next/router";
import {ApiParamKey} from "../api/network/ApiParamKey";
import {ApiServices} from "../api/network/ApiServices";
import {fetchData} from "../api/redux/about/certification";
import {wrapper} from "../api/store";

const MyComponent = (props) => {

    // animation
    const dispatch = useDispatch()
    const getData = useSelector(state => state.certificateReducer)




    // data refactor
    const bannerImage = getData?.data?.sections?.find(f => f?.page_data?.slug === "cerification-banner");
    const certificates = getData?.data?.sections?.filter(f => f?.page_data?.slug !== "cerification-banner");

    console.log(certificates?.[0]?.posts?.list);

    const router = useRouter();

    useEffect(() => {

        if (!props.isServer) {
            let param = {
                [ApiParamKey.page_id]: '297',
                [ApiParamKey.get_section]: 'true'
            }
            let api_services = ApiServices.SECTIONS;
            dispatch(fetchData([api_services, param]))
        }
    }, [props.isServer, router])
    return (
        <StyledComponent>
            <NextSeo
                title={'Certifications | GPH Ispat Limited'}
                description={'GPH Ispat Ltd. One of the leaders of Bangladesh in manufacturing steel promises a super strong future and economy with its world –class products. Not only structural bar, but GPH Ispat Ltd. is also one of the producers of low &amp; medium carbon and low alloy steel billets in Bangladesh, the main ingredient of manufacturing graded steel bar. As GPH is ensuring the highest quality products in Bangladesh as per various international and national standards, GPH steel billets and Bars is getting exported to other countries after nourishing national demand. The introduction of GPH Ispat Ltd. has all the potentials to take Bangladesh quite a few steps forward to a stronger, brighter tomorrow'}
            />
            <InnerBanner title={bannerImage?.page_data?.short_desc} img={bannerImage?.images?.list[0]?.full_path}
                         subtitle={bannerImage?.page_data?.subtitle} des={bannerImage?.page_data?.description}/>
            <Certifications data={certificates?.[0]?.posts?.list}/>
        </StyledComponent>
    );
};

const StyledComponent = styled.section`

`;
export const getServerSideProps = wrapper.getServerSideProps(
    (store) => async ({req}) => {

        let param = {
            [ApiParamKey.page_id]: '297',
            [ApiParamKey.get_section]: 'true'
        }

        const isServer = !req.url.startsWith("/_next");

        if (isServer) {
            let api_services = ApiServices.SECTIONS;
            await store.dispatch(fetchData([api_services, param]))
        }
        return {
            props: {
                isServer,
                title: "about",
            },
        };
    })

export default MyComponent;
