import {createSlice} from "@reduxjs/toolkit";
import {createAsyncThunk} from "@reduxjs/toolkit";
import {get} from "../../network/AxiosService";

const initialState = {
    loading: false,
    data: [],
    global:[],
    error: "",
};

export const fetchData = createAsyncThunk("menu", (params) => {
    return get(params);
});

export const fetchGlobalSettings = createAsyncThunk("settings", (params) => {
    return get(params);
});

const dataSlice = createSlice({
    name: "menu",
    initialState,
    extraReducers: (builder) => {
        builder.addCase(fetchData.pending, (state) => {
            state.loading = true;
            state.data = [];
            state.error = "";
        });
        builder.addCase(fetchData.fulfilled, (state, action) => {
            state.loading = false;
            state.data = action.payload;
            state.error = "";
        });
        builder.addCase(fetchData.rejected, (state, action) => {
            state.loading = false;
            state.data = [];
            state.error = action.error;
        });
        builder.addCase(fetchGlobalSettings.pending, (state) => {
            state.loading = true;
            state.global = [];
            state.error = "";
        });
        builder.addCase(fetchGlobalSettings.fulfilled, (state, action) => {
            state.loading = false;
            state.global = action.payload;
            state.error = "";
        });
        builder.addCase(fetchGlobalSettings.rejected, (state, action) => {
            state.loading = false;
            state.global = [];
            state.error = action.error;
        });

    }
});

export default dataSlice.reducer;
