import React from 'react';
import styled from "styled-components";
import {Container, Row, Col} from "react-bootstrap";
import Link from 'next/link'
import Title from "../Title";
import {Img} from "../Img";
// import pdf from '../../public/file/Accreditation.pdf'

const MyComponent = ({data}) => {

    return (
        <StyledComponent className={'certification pt-160 pb-120'}>
            <Container>
                <Row>
                    {/*<Col sm={12}>*/}
                    {/*    <Title margin={' 0 0 60px 0'} text={'certifications'}/>*/}
                    {/*</Col>*/}

                    {
                        data && data?.length>0 &&
                        data.map((element)=>{
                            return(
                                <Col sm={3}>
                                    <div className="certification__single">
                                        {
                                            element.images.map((f, idx) => {
                                                if (f.is_thumb === "on") {
                                                    return (
                                                        <Link href={f.full_path}><a
                                                            target="_blank"
                                                        ></a></Link>
                                                    );
                                                }
                                                return null; // Added default return statement
                                            })
                                        }

                                        <div className="certification__single__img">
                                            {
                                                element.images.map((f, idx) => {
                                                    if (f.is_thumb !== "on") {
                                                        return (
                                                            <Img key={idx} src={f.full_path} />
                                                        );
                                                    }
                                                    return null; // Added default return statement
                                                })
                                            }

                                        </div>
                                        <p>{element?.data?.title}</p>
                                    </div>
                                </Col>
                            )
                        })
                    }



                </Row>
            </Container>
        </StyledComponent>
    );
};

const StyledComponent = styled.section`
  .col-sm-4{
    margin-bottom: 30px;
  }
  .certification__single {
    width: 85%;
    position: relative;
    //border-bottom:1px solid black;
    height: 100%;
    a {
      position: absolute;
      inset: 0;
      z-index: 2;
    }

    &__img {
      padding-top: 60%;
      position: relative;
      background-color: #DDD;
      border:1px solid black;
    }
    
    p{
      font-size: 17px;
      font-weight: 500;
      padding-top: 20px;
      text-align: center;
      padding-bottom: 20px;
    }
  }
  
  @media(min-width: 1600px){
    .col-sm-4{
      max-width: 25%;
    }
  }
  
  @media(max-width: 768px){
    .col-sm-4{
      min-width: 50%;
    }
  }
`;

export default MyComponent;
