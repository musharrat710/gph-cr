import React from 'react';
import styled from "styled-components";
import {Container, Row, Col} from "react-bootstrap";
import Link from 'next/link'
import {Img} from "../Img";
import Title from "../Title";
import {hover} from "../../styles/globalStyleVars";
import SingleAwards from "./SingleAwards";
import MediaEvents from "../NewsEvents";

const MyComponent = ({data}) => {
    return (
        <StyledComponent className={'awards.js pt-150 pb-120'}>
            <Container>
                <Row>
                    {
                        data && data?.length>0 &&
                        data.map((element)=>{
                            return(
                                <Col md={6} lg={4} xs={12} className='fade-up'>
                                    <SingleAwards key={element?.id}
                                                  data={element}
                                                  src={element?.images?.[0]?.full_path}
                                    />
                                </Col>
                            )
                        })
                    }
                </Row>
            </Container>
        </StyledComponent>
    );
};

const StyledComponent = styled.section`

  .awards__wrap {
    margin-bottom: 50px;
    padding-bottom: 50px;
    position: relative;

    &:nth-last-of-type(1) {
      margin-bottom: 0;
      padding-bottom: 0;

      &:after {
        display: none;
      }
    }

    &:after {
      content: '';
      left: 15px;
      right: 15px;
      height: 1px;
      background-color: rgba(251, 3, 12, 0.2);
      position: absolute;
      bottom: 0;
    }
  }

  .awards__wrap__img {
    padding-top: 60%;
    position: relative;
  }

  .awards__content {
    padding-left: 50px;

    h4 {
      margin-bottom: 30px;
    }
  }
  
  @media(max-width: 991px){
    .awards__content{
      min-width: 100%;
      padding-left: 15px;
      margin-top: 30px;
      h4{
        font-size: 20px;
      }
    }
  }

`;

export default MyComponent;
