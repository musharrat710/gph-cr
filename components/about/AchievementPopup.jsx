import React from 'react';
import styled from 'styled-components';
import Modal from "react-bootstrap/Modal";
import { Col, Container, Row } from "react-bootstrap";
import SimpleBar from 'simplebar-react';
import 'simplebar/dist/simplebar.min.css';
import ReactHtmlParser from "react-html-parser";
import { Img } from "../Img";

const Popup = ({ show, handleClose, item }) => {
    let slider_enb = item?.images?.length;
    let img = item?.images?.find(f => f?.ext === "jpg");

    return (
        <StyledModal slider_enb={slider_enb}>
            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
                className="gph_modal"
            >
                <Modal.Header closeButton>
                    <Container>
                        <Row>
                            <Col className="header_wrap">
                                <Modal.Title>{item?.data?.title}</Modal.Title>
                                <div onClick={handleClose} className="close_button">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="21.414" height="21.414" viewBox="0 0 21.414 21.414">
                                        <g id="Group_13674" data-name="Group 13674" transform="translate(-1224.793 -42.793)">
                                            <line id="Line_1" data-name="Line 1" x2="28.284" transform="translate(1225.5 43.5) rotate(45)" fill="none" stroke="#222" stroke-linecap="round" stroke-width="1" />
                                            <line id="Line_2" data-name="Line 2" x2="28.284" transform="translate(1225.5 63.5) rotate(-45)" fill="none" stroke="#222" stroke-linecap="round" stroke-width="1" />
                                        </g>
                                    </svg>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </Modal.Header>
                <SimpleBar className="main_scroll" style={{ maxHeight: '82vh' }}>
                    <Modal.Body>
                        <Container>
                            <Row>
                                <Col md={12}>
                                    <div className='top_img'>
                                        <Img src={img?.full_path} />
                                    </div>
                                </Col>
                                <Col md={12}>
                                    <div className="left_col__content">
                                        <p>{item?.data?.short_desc}</p>
                                        {ReactHtmlParser(item?.data?.description)}
                                    </div>
                                    {/*<SimpleBar className="custombar" style={{ maxHeight: '70vh' }}>*/}
                                    {/*   */}
                                    {/*</SimpleBar>*/}
                                </Col>
                            </Row>
                        </Container>
                    </Modal.Body>
                </SimpleBar>
            </Modal>
        </StyledModal>
    );
};

const StyledModal = styled.div`
  .modal-dialog {
    margin: 0;
  }

  
  
  .modal-body {
    padding: 0;
  }

  .header_wrap {
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
  }

  .close_button {
    cursor: pointer;
  }

  .top_img {
    margin-bottom: 15px;
  }

  .left_col__content {
    padding: 15px;
  }

  .main_scroll, .custombar {
    overflow-y: auto;
  }
`;

export default Popup;
